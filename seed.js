// Archivo seed.js
const mongoose = require('mongoose');

// Imporatmos el modelo Pet en este nuevo archivo.
const Player = require('./models/Players.js');
const Trainer = require('./models/Trainers.js');

const players = [
  {
    name: 'Marco Reus',
    age: 31,
    position: 'Medio Ofensivo',
    number: 11
  },
  {
    name: 'Neymar Jr',
    age: 28,
    position: 'Delantero-Extremo',
    number: 10
  },
  {
    name: 'Paulo Dybala',
    age: 27,
    position: 'Medio Ofensivo',
    number: 10
  },
  {
    name: 'Lionel Messi',
    age: 33,
    position: 'Extremo',
    number: 10
  },
  {
    name: 'Cristiano Ronaldo',
    age: 35,
    position: 'Extremo-Delantero',
    number: 7
  },
];

const trainers = [
    {
        name: 'Pep Guardiola',
        age: 49
    },
    {
        name: 'José Mourinho',
        age: 57
    },
    {
        name: 'Zinedine Zidane',
        age: 48
    },
    {
        name: 'Carlo Ancelotti',
        age: 61
    },
    {
        name: 'Jürgen Klopp',
        age: 53
    },
];

const trainersDocuments = trainers.map(trainer => new Trainer(trainer));
const playersDocuments = players.map(player => new Player(player));

mongoose
  .connect('mongodb://localhost:27017/proyecto_node', {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
  .then(async () => {
		// Utilizando Pet.find() obtendremos un array con todos los pets de la db
    const allPlayers = await Player.find();
    const allTrainers = await Trainer.find();
		// Si existen pets previamente, dropearemos la colección
    if (allPlayers.length && allTrainers.length) {
      await Player.collection.drop();
      await Trainer.collection.drop();
    }
  })
  .catch((err) => console.log(`Error deleting data: ${err}`))
  .then(async () => {
		// Una vez vaciada la db de las mascotas, usaremos el array petDocuments
		// para llenar nuestra base de datos con todas las mascotas.
        await Player.insertMany(playersDocuments);
        await Trainer.insertMany(trainersDocuments);
	})
  .catch((err) => console.log(`Error creating data: ${err}`))
	// Por último nos desconectaremos de la DB.
  .finally(() => mongoose.disconnect());