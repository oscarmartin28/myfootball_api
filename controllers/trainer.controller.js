const Trainer = require('../models/Trainers.js');

const trainerGet = async (req, res) => {
    try {
        const trainers = await Trainer.find();
        return res.status(200).render('trainers', {title: 'Entrenadores', trainers });
    } catch (err) {
        return res.status(500).json(err);
    }
};

const idGet = async (req, res) => {
    const id = req.params.id;
  
    try {
      const trainerFinded = await Trainer.findById(id);
      return res.status(200).json(trainerFinded);
  
    } catch (error) {
      return res.status(500).json(error);
    }
  };

const createPost = async (req, res, next) => {
    console.log(req.file);
    const trainerPicture = req.file_url;
    try {
      // Crearemos una instancia de mascota con los datos enviados
      const newTrainer = new Trainer({
        name: req.body.name,
        age: req.body.age,
        picture: trainerPicture,
      });
  
      await newTrainer.save();
      return res.redirect('/trainers');
    } catch (err) {
          // Lanzamos la función next con el error para que gestione todo Express
      next(err);
    }
  };

const editPut = async (req, res, next) => {
    try {
      const id = req.body.id;
  
      const updatedTrainer = await Trainer.findByIdAndUpdate(
        id, // La id para encontrar el documento a actualizar
        { 
            name: req.body.name,
            age: req.body.age
        },
         // Campos que actualizaremos
        { new: true } // Usando esta opción, conseguiremos el documento actualizado cuando se complete el update
      );
  
      return res.status(200).json(updatedTrainer);
    } catch (err) {
      next(err);
    }
  };

const trainerDelete = async (req, res, next) => {
    const { id } = req.params;
  
    try {
      await Trainer.findByIdAndDelete(id);
  
      res.status(200).json("Trainer deleted");
    } catch (error) {
      next(error);
    }
  };

module.exports = {
    trainerGet,
    idGet,
    createPost,
    editPut,
    trainerDelete,
}

