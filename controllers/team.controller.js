const Team = require('../models/Teams');
const Player = require('../models/Players');
const Trainer = require('../models/Trainers');

const teamGet = async (req, res, next) => {
    try {
      const teams = await Team.find().populate('players').populate('trainers');
  
      return res.status(200).render('teams', { title: 'teams', teams });
    } catch (err) {
      next(err);
    }
  };

const teamCreate = async (req, res, next) => {
    console.log(req.file);
    const teamPicture = req.file_url;
  try {
    const newteam = new Team({
      name: req.body.name,
      location: req.body.location,
      picture: teamPicture,
    });

    await newteam.save();
    return res.redirect('/teams');
  } catch (err) {
    next(err);
  }
};

const addPlayer = async (req, res, next) => {
    try {
      const team = req.body.team;
      const player = req.body.player;
  
      console.log(team);
      console.log(player);
  
      const playerId = await Player.find({name: player}).select('_id');
      const teamId = await Team.find({name: team}).select('_id');
      console.log('playerId', playerId);
      console.log('teamId', teamId);
      const updatedTeam = await Team.findByIdAndUpdate(
        teamId[0]._id,
        { $push: { players: playerId[0]._id } },
        { new: true }
      );
  
      return res.redirect('/teams'); 
    
    } catch (err) {
      next(err);
    }
};

const addTrainer = async (req, res, next) => {
    try {
      const team = req.body.team;
      const trainer = req.body.trainer;
  
      console.log(team);
      console.log(trainer);
  
      const trainerId = await Trainer.find({name: trainer}).select('_id');
      const teamId = await Team.find({name: team}).select('_id');
      console.log('trainerId', trainerId);
      console.log('teamId', teamId);
      const updatedTeam = await Team.findByIdAndUpdate(
        teamId[0]._id,
        { $push: { trainers: trainerId[0]._id } },
        { new: true }
      );
  
      return res.redirect('/teams'); 
    
    } catch (err) {
      next(err);
    }
};

module.exports = {
    teamGet,
    teamCreate,
    addPlayer,
    addTrainer,
}