

const indexGet = (req, res, next) => {
    res.status(200).render('index', { title: 'MyFutbol' });
}

const registerGet =  (req, res, next) => {
    res.render('register');
};

const loginGet = (req, res, next) => {
    res.render('login');
};

const newPlayerGet = (req, res, next) => {
    res.render('new-player');
};

const newTrainerGet = (req, res, next) => {
    res.render('new-trainer');
};

const newTeamGet = (req, res, next) => {
    res.render('new-team');
};

const addPlayerGet = (req, res, next) => {
    res.render('add-player');
};

const addTrainerGet = (req, res, next) => {
    res.render('add-trainer');
};

module.exports = {
    indexGet,
    registerGet,
    loginGet,
    newPlayerGet,
    newTrainerGet,
    newTeamGet,
    addPlayerGet,
    addTrainerGet,
};