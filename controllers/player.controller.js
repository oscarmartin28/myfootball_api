const Player = require('../models/Players.js');

const playerGet = async (req, res) => {
    try {
        const players = await Player.find();
        return res.status(200).render('players', {title: 'Jugadores', players });
    } catch (err) {
        return res.status(500).json(err);
    }
};

const idGet = async (req, res) => {
    const id = req.params.id;
  
    try {
      const playerFinded = await Player.findById(id);
      return res.status(200).json(playerFinded);
  
    } catch (error) {
      return res.status(500).json(error);
    }
};

const createPost = async (req, res, next) => {
    console.log(req.file);
    const playerPicture = req.file_url;
  
      try {
        // Crearemos una instancia de mascota con los datos enviados
        const newPlayer = new Player({
          name: req.body.name,
          age: req.body.age,
          position: req.body.position,
          number: req.body.number,
          picture: playerPicture,
        });
    
        // Guardamos la mascota en la DB
        await newPlayer.save();
        return res.redirect('/players');
      } catch (err) {
            // Lanzamos la función next con el error para que gestione todo Express
        next(err);
      }
};

const editPut = async (req, res, next) => {
    try {
      const id = req.body.id;
  
      const updatedPlayer = await Player.findByIdAndUpdate(
        id, // La id para encontrar el documento a actualizar
        { 
            name: req.body.name,
            age: req.body.age,
            position: req.body.position,
            number: req.body.number 
        },
         // Campos que actualizaremos
        { new: true } // Usando esta opción, conseguiremos el documento actualizado cuando se complete el update
      );
  
      return res.status(200).json(updatedPlayer);
    } catch (err) {
      next(err);
    }
};

const playerDelete = async (req, res, next) => {
    const { id } = req.params;
  
    try {
      await Player.findByIdAndDelete(id);
  
      res.status(200).json("Player deleted");
    } catch (error) {
      next(error);
    }
};

module.exports = {
    playerGet,
    idGet,
    createPost,
    editPut,
    playerDelete,
}




