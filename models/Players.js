const mongoose = require('mongoose');

const Schema = mongoose.Schema;

// Creamos el esquema de mascotas
const playerSchema = new Schema(
  {
    name: { type: String, required: true },
    age: { type: Number },
    position: { type: String, required: true },
    number: { type: Number, required: true},
    picture: { type: String },
  },
  {
    // Esta propiedad servirá para guardar las fechas de creación y actualización de los documentos
    timestamps: true,
  }
);

// Creamos y exportamos el modelo Pet
const Player = mongoose.model('Player', playerSchema);
module.exports = Player;