const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const teamSchema = new Schema(
  {
    name: { type: String, required: true },
    location: { type: String, required: true },
    picture: { type: String },
		// Tipo mongoose Id y referencia al modelo Pet
    players: [{ type: mongoose.Types.ObjectId, ref: 'Player' }],
    trainers: [{ type: mongoose.Types.ObjectId, ref: 'Trainer' }],
  },
  {
    timestamps: true,
  }
);

const Team = mongoose.model('Team', teamSchema);
module.exports = Team;