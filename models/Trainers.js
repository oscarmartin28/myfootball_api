const mongoose = require('mongoose');

const Schema = mongoose.Schema;

// Creamos el esquema de mascotas
const trainerSchema = new Schema(
  {
    name: { type: String, required: true },
    age: { type: Number },
    picture: { type: String },
  },
  {
    // Esta propiedad servirá para guardar las fechas de creación y actualización de los documentos
    timestamps: true,
  }
);

// Creamos y exportamos el modelo Pet
const Trainer = mongoose.model('Trainer', trainerSchema);
module.exports = Trainer;