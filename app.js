require('dotenv').config();

const express = require('express');
require('./db.js');
const path = require('path');
const passport = require('passport');
const mongoose = require('mongoose');
const session = require('express-session');
const MongoStore = require('connect-mongo')(session);
require('./passport'); // Requerimos nuestro archivo de configuración

const playerRoutes = require('./routes/player.routes');
const trainerRoutes = require('./routes/trainer.routes');
const teamRoutes = require('./routes/team.routes');
const indexRoutes = require('./routes/index.routes');
const userRouter = require('./routes/user.routes');
const authMiddleware = require('./middlewares/auth.middleware');

const PORT = process.env.PORT || 5000;

const server = express();

server.set('views', path.join(__dirname, 'views'));
server.set('view engine', 'hbs');

server.use(express.json());
server.use(express.urlencoded({ extended: false }));
server.use(express.static(path.join(__dirname, 'public')));

server.use(
    session({
      secret: process.env.SESSION_SECRET || 'upgradehub_node', // ¡Este secreto tendremos que cambiarlo en producción!
      resave: false, // Solo guardará la sesión si hay cambios en ella.
      saveUninitialized: false, // Lo usaremos como false debido a que gestionamos nuestra sesión con Passport
      cookie: {
        maxAge: 3600000 // Milisegundos de duración de nuestra cookie, en este caso será una hora.
      },
      store: new MongoStore({ mongooseConnection: mongoose.connection }),
    })
  );
server.use(passport.initialize());
server.use(passport.session());

server.use('/players', [authMiddleware.isAuthenticated], playerRoutes);
server.use('/trainers', [authMiddleware.isAuthenticated], trainerRoutes);
server.use('/users', userRouter);
server.use('/teams', [authMiddleware.isAuthenticated], teamRoutes);
server.use('/', indexRoutes);

server.use('*', (req, res, next) => {
    const error = new Error('Route not found'); 
    error.status = 404;
    next(error); // Lanzamos la función next() con un error
  });
  
  server.use((err, req, res, next) => {
    return res.status(err.status || 500).render('error', {
      message: err.message || 'Unexpected error',
      status: err.status || 500,
    });
  });

server.listen(PORT, () => {
    console.log(`Server running in http://localhost:${PORT}`);
});