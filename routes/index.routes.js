const express = require('express');
const indexController = require('../controllers/index.controller');
const router = express.Router();
const authMiddleware = require('../middlewares/auth.middleware');

router.get('/', indexController.indexGet);

router.get('/register', indexController.registerGet);

router.get('/login', indexController.loginGet);

router.get('/new-player', [authMiddleware.isAuthenticated], indexController.newPlayerGet);

router.get('/new-trainer', [authMiddleware.isAuthenticated], indexController.newTrainerGet);

router.get('/new-team', [authMiddleware.isAuthenticated], indexController.newTeamGet);

router.get('/add-player', [authMiddleware.isAuthenticated], indexController.addPlayerGet);

router.get('/add-trainer', [authMiddleware.isAuthenticated], indexController.addTrainerGet);

module.exports = router;