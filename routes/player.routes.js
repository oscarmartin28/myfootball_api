const express = require('express');

const playerController = require('../controllers/player.controller');

const fileMiddlewares = require('../middlewares/file.middleware');

const cloudinary = require('../middlewares/file.middleware');

const router = express.Router();

router.get('/', playerController.playerGet);

router.get("/:id", playerController.idGet);

router.post('/create', [fileMiddlewares.upload.single('picture'), cloudinary.uploadToCloudinary], playerController.createPost);

  router.put('/edit', playerController.editPut);

  router.delete("/:id", playerController.playerDelete);


module.exports = router;