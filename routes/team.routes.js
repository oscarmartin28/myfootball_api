const express = require('express');

const teamController = require('../controllers/team.controller');

const cloudinary = require('../middlewares/file.middleware');

const fileMiddlewares = require('../middlewares/file.middleware');

const router = express.Router();

router.get('/', teamController.teamGet);

router.post('/create', [fileMiddlewares.upload.single('picture'), cloudinary.uploadToCloudinary], teamController.teamCreate);

router.post('/add-player',  teamController.addPlayer);

router.post('/add-trainer', teamController.addTrainer);

module.exports = router;