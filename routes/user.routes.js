// En el archivo user.routes.js
const express = require('express');
const passport = require('passport');

const userController = require('../controllers/user.controller');

require('../passport.js');
const router = express.Router();

router.post('/register', userController.registerPost);

router.post('/login', userController.loginPost);

router.post('/logout', userController.logoutPost);

module.exports = router;