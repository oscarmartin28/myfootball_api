const express = require('express');

const Trainer = require('../models/Trainers.js');

const trainerController = require('../controllers/trainer.controller');

const cloudinary = require('../middlewares/file.middleware');

const fileMiddlewares = require('../middlewares/file.middleware');

const router = express.Router();

router.get('/', trainerController.trainerGet);

router.get("/:id", trainerController.idGet);

router.post('/create', [fileMiddlewares.upload.single('picture'), cloudinary.uploadToCloudinary], trainerController.createPost);

router.put('/edit', trainerController.editPut);

router.delete("/:id", trainerController.trainerDelete);

module.exports = router;